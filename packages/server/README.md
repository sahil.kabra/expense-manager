[![pipeline status](https://gitlab.com/sahil.kabra/expense-manager/badges/master/pipeline.svg)](https://gitlab.com/sahil.kabra/expense-manager/commits/master)

# expense manager
A very simple app to manage expenses.

## Getting started
### Development
#### System Requirements
1. node
1. yarn
1. mongo

#### Useful Commands
Rename `.env.example` to `.env` and customize as needed.
1. `yarn run build && yarn run start` to run the app
1. `yarn run start:dev` to run the app in dev mode
1. `yarn run start:debug` to run the app in debug mode
1. `yarn run test` to run the tests
