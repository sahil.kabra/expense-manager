/**
 * The only place where process is used.
 * Stores any env configuration that is required.
 * The app will get all properties from this object
 */
export const config = {
    appName: 'expenses',
    express: {
        port: process.env.PORT || 9001,
    },
    mongo: {
        dbName: process.env.MONGODB || 'expenses',
        get: (key: string) => process[key],
        host: process.env.MONGOHOST || 'localhost',
        pid: process.pid,
        port: process.env.MONGOPORT || 27017,
        url: createUri,
    },
};

function createUri() {
    if (process.env.MONGODB_URI) {
        return process.env.MONGODB_URI;
    }
    return `mongodb://${config.mongo.host}:${config.mongo.port}/${
        config.mongo.dbName
    }`;
}
