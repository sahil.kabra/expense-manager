import mongoose from 'mongoose';

import { config } from '../../config';
import { logger } from '../../logger';

export function connectToMongo() {
    const mongoUrl = config.mongo.url();
    mongoose.connect(
        mongoUrl,
        { useNewUrlParser: true }
    );
    mongoose.connection.on('error', () => {
        logger.error(`unable to connect to mongo db on ${mongoUrl}`);
    });
}
