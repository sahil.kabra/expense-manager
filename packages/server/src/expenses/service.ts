/**
 * Services related to cars.
 */
import { logger } from '../logger';
import {
  model as expenseModel,
  Expense,
  ExpenseUpdate,
  Filters,
} from './model';

const errorHandler = (err: any) => {
  logger.error(`Unable to save expense: ${err}`);
};

/**
 * Saves a new expense to the DB.
 *
 * @param expense: The expense to be saved to the DB.
 * @return: The id of the newly saved car.
 */
export function createExpense(expenses: Expense | Expense[]) {
  const exp = Array.isArray(expenses) ? expenses : [expenses];
  return Promise.all(exp.map(e => expenseModel().create(e))).catch(
    errorHandler
  );
}

export function listAllExpenses() {
  return expenseModel().listAll();
}

export function getExpense(id: string) {
  return expenseModel().getById(id);
}

export function fetchExpenses(filters: Filters) {
  return expenseModel().find(filters);
}

export function deleteExpense(id: string) {
  return expenseModel().delete(id);
}

export function updateExpense(id: string, updates: ExpenseUpdate) {
  return expenseModel().update(id, updates);
}

export { Expense, ExpenseUpdate, Filters };
