/**
 * This represents whether this record is debit or credit.
 * Debit: Amount is deducted from account.
 * Credit: Amount is added to account.
 */
export enum ExpenseType {
  DEBIT = 'debit',
  CREDIT = 'credit',
}

export type Expense = {
  amount: number;
  date: Date;
  description?: string;
  id?: string; // this will be null for an expense that has not been saved
  labels: string[]; // metadata for the expense that could be used to search
  type: ExpenseType;
};
export type ExpenseUpdate = {
  amount?: number;
  date?: Date;
  description?: string;
  labels?: string[]; // metadata for the expense that could be used to search
  type?: ExpenseType;
};

export type Filters = {
  labels?: string | string[];
  date?: Date;
  type?: ExpenseType;
};

export interface ExpenseModel {
  create(expense: Expense): Promise<Expense>;
  find(filters: Filters): Promise<Expense[]>;
  getById(id: Expense['id']): Promise<Expense>;
  listAll(): Promise<Expense[]>;
  delete(id: Expense['id']): Promise<void>;
  update(id: Expense['id'], updates: ExpenseUpdate): Promise<Expense>;
}
