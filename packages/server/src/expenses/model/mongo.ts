import mongoose from 'mongoose';

import { logger } from '../../logger';
import { Expense, ExpenseModel, ExpenseUpdate, Filters } from './types';

export type ExpenseSchema = mongoose.Document &
  Expense & {
    _id: number;
    createdDate: Date;
  };

const Schema = mongoose.Schema;

const ExpenseSchema: mongoose.Schema = new Schema({
  amount: { type: Number, default: 0 },
  createdDate: { type: Date, default: Date.now },
  date: { type: Date, default: Date.now },
  description: { type: String },
  labels: { type: [String] },
  type: { type: String },
});

ExpenseSchema.virtual('url').get(function(this: ExpenseSchema) {
  return `/expenses/${this._id}`;
});

const ExpenseDbModel: mongoose.Model<ExpenseSchema> = mongoose.model<
  ExpenseSchema
>('expense', ExpenseSchema);

const schemaToModel = (expense: ExpenseSchema) => ({
  amount: expense.amount,
  createdDate: expense.createdDate,
  date: expense.date,
  description: expense.description,
  id: expense._id,
  labels: expense.labels,
  type: expense.type,
});

const filterToModel = (filters: Filters) => {
  if (!filters) {
    return {};
  }
  return Object.keys(filters).reduce((f: any, k: string) => {
    if (filters[k] !== undefined) {
      if (Array.isArray(filters[k])) {
        f[k] = { $in: filters[k] };
      } else {
        f[k] = filters[k];
      }
    }
    return f;
  }, {});
};

export class ExpenseMongoModel implements ExpenseModel {
  public create(expense: Expense) {
    return ExpenseDbModel.create({ ...expense }).then(schemaToModel);
  }
  public listAll() {
    return this.find({});
  }
  public getById(id: Expense['id']) {
    return ExpenseDbModel.findById(id).then(schemaToModel);
  }
  public find(filters: Filters) {
    return ExpenseDbModel.find(filterToModel(filters)).then(
      (expenses: Expense[]) => expenses.map(schemaToModel)
    );
  }
  public delete(id: Expense['id']) {
    return ExpenseDbModel.deleteOne({ _id: id }, (err: any) => {
      logger.info(`error while deleting id ${id}: ${err}`);
    });
  }
  public update(id: Expense['id'], updates: ExpenseUpdate) {
    return ExpenseDbModel.findByIdAndUpdate(
      { _id: id },
      { $set: updates },
      (err: any, expense: Expense) => {
        if (err) {
          logger.info(`error while updating id ${id}: ${err}`);
        }
        return expense;
      }
    );
  }
}
