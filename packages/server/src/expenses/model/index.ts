import { ExpenseMongoModel } from './mongo';
import {
  Expense,
  ExpenseModel,
  ExpenseType,
  ExpenseUpdate,
  Filters,
} from './types';

export const model: () => ExpenseModel = (() => {
  let m: ExpenseModel;
  return () => {
    if (!m) {
      m =
        process.env.DB_MODE === 'mongo'
          ? new ExpenseMongoModel()
          : new ExpenseMongoModel();
    }
    return m;
  };
})();

export { Expense, ExpenseType, ExpenseModel, ExpenseUpdate, Filters };
