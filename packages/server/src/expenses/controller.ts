/**
 * The controller.
 * It gets params from the request and return response.
 */
import { Request, Response } from 'express';

import { logger } from '../logger';
import * as ExpenseService from './service';

export const ExpenseController = {
  create: async (request: Request, response: Response) => {
    const expenses: ExpenseService.Expense[] = [].concat(request.body);
    const savedExpenses = await ExpenseService.createExpense(expenses);
    response.send(savedExpenses);
  },
  deleteExpense: async (request: Request, response: Response) => {
    const id = request.params.id;
    await ExpenseService.deleteExpense(id);
    response.send();
  },
  getExpense: async (request: Request, response: Response) => {
    const id = request.params.id;
    const expenses = await ExpenseService.getExpense(id);
    response.send(expenses);
  },
  getExpenses: async (request: Request, response: Response) => {
    const filters: ExpenseService.Filters = {
      date: request.query.date,
      labels: request.query.labels,
      type: request.query.type,
    };
    logger.info(`querying for expenses for: ${JSON.stringify(filters)}`);
    const expenses = filters
      ? await ExpenseService.fetchExpenses(filters)
      : await ExpenseService.listAllExpenses();
    response.send(expenses);
  },
  updateExpense: async (request: Request, response: Response) => {
    const id = request.params.id;
    const updates: ExpenseService.ExpenseUpdate = request.body;
    await ExpenseService.updateExpense(id, updates);
    response.send();
  },
};
