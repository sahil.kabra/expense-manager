import * as dotenv from 'dotenv';
import { config } from './config';
import { connectToMongo } from './db/mongo/connection';
import { logger } from './logger';
import server from './rapi/server';

dotenv.config();
if (process.env.DB_MODE === 'mongo') {
    connectToMongo();
}

server.listen(config.express.port, (err: any) => {
    if (err) {
        return logger.error(err);
    }

    return logger.info(`server is listening on ${config.express.port}`);
});
