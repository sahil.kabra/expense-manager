import { Button, InputLabel, MenuItem } from '@material-ui/core';
import * as React from 'react';

import { config } from '../../config';
import { Expense, ExpenseType } from '../../types';
import {
  Amount,
  Container,
  DatePicker,
  Description,
  Form,
  FormControl,
  Type
} from './index.style';

export interface Props {
  onClose: () => void;
}
type State = Expense;

class AddExpense extends React.Component<Props, State> {
  public constructor(props: Props) {
    super(props);
    this.state = {
      amount: 0,
      date: new Date(),
      description: '',
      labels: [] as string[], // metadata for the expense that could be used to search
      type: ExpenseType.DEBIT
    };
  }
  public render() {
    const date = this.state.date;
    return (
      <Container>
        <Form>
          <FormControl>
            <InputLabel htmlFor='type'>Expense type</InputLabel>
            <Type
              inputProps={{ id: 'type' }}
              value={this.state.type}
              onChange={this.handleTypeChange}
            >
              <MenuItem value=''>
                <em>None</em>
              </MenuItem>
              <MenuItem value='debit'>Debit</MenuItem>
              <MenuItem value='credit'>Credit</MenuItem>
            </Type>
          </FormControl>
          <Amount
            id='amount'
            label='Amount'
            value={this.state.amount}
            onChange={this.handleAmountChange}
            type='number'
            margin='normal'
          />
          <Description
            id='description'
            label='Description'
            value={this.state.description}
            onChange={this.handleDescriptionChange}
            margin='normal'
          />
          <DatePicker
            label='Date'
            value={`${date.getFullYear()}-${date.getMonth()}-${date.getDate()}`}
            onChange={this.handleDateChange}
          />
          <Button onClick={this.handleAddExpense} variant='contained'>
            Add
          </Button>
        </Form>
      </Container>
    );
  }

  private handleTypeChange = (event: React.FormEvent<HTMLInputElement>) => {
    this.setState({
      type: ExpenseType[event.currentTarget.value]
    });
  };

  private handleAmountChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    this.setState({
      amount: Number(event.currentTarget.value)
    });
  };

  private handleDescriptionChange = (
    event: React.ChangeEvent<HTMLInputElement>
  ) => {
    this.setState({
      description: event.currentTarget.value
    });
  };
  private handleDateChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    this.setState({
      date: new Date(event.currentTarget.value)
    });
  };

  private handleAddExpense = () => {
    const expense = this.state;
    const myHeaders = new Headers();
    myHeaders.append('Content-Type', 'application/json');
    fetch(`${config.apiUrl}/expenses`, {
      body: JSON.stringify(expense),
      headers: myHeaders,
      method: 'POST',
      mode: 'cors'
    });
  };
}

export default AddExpense;
