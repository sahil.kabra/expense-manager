import {
    FormControl as MFormControl,
    Paper,
    Select,
    TextField
} from '@material-ui/core';
import styled from 'styled-components';

export const Container = styled(Paper)`
    padding: 1em;
`;
export const Type = styled((props: any) => <Select {...props} />)``;
export const Description = styled((props: any) => <TextField {...props} />)``;
export const FormControl = styled(MFormControl)`
    & {
        min-width: 1.2em;
    }
`;
export const Form = styled.form``;
export const Amount = styled((props: any) => <TextField {...props} />)``;
export const DatePicker = styled((props: any) => (
    <TextField type='date' {...props} />
))``;
