import 'react-table/react-table.css';

import moment from 'moment';
import * as React from 'react';
import ReactTable, { Column } from 'react-table';

import { config } from '../../config';
import { Expense } from '../../types';

interface State {
    data: Expense[];
    error: Error | null;
    isLoading: boolean;
}
export default class ExpenseList extends React.Component<{}, State> {
    public constructor(props: {}) {
        super(props);
        this.state = {
            data: [],
            error: null,
            isLoading: true
        };
    }
    public componentDidMount() {
        fetch(`${config.apiUrl}/expenses`)
            .then(response => response.json())
            .then(data => {
                this.setState({
                    data: Array.isArray(data) ? data : [],
                    error: null,
                    isLoading: false
                });
            })
            .catch((e: any) => {
                this.setState({
                    error: e,
                    isLoading: false
                });
            });
    }
    public render() {
        const { error, isLoading, data } = this.state;
        if (isLoading) {
            return <div>Loading...</div>;
        } else if (!isLoading && error) {
            return <div>Error {`${error.message}`}</div>;
        } else if (!isLoading && !error) {
            return (
                <ReactTable
                    data={data}
                    columns={columns}
                    defaultSorted={sorting}
                    className='-striped -highlight'
                />
            );
        } else {
            return null;
        }
    }
}

const columns: Column[] = [
    {
        Cell: (row: { original: Expense }) =>
            moment(row.original.date).format('YYYY/MM/DD'),
        Header: 'date'
    },
    {
        Header: 'expense',
        accessor: 'description'
    },
    {
        Header: 'type',
        accessor: 'type'
    },
    {
        Header: 'amount',
        accessor: 'amount'
    }
];

const sorting = [
    {
        desc: true as any,
        id: 'date'
    }
];
