import getConfig from 'next/config';

const nextConfig = getConfig();
export const config = {
    apiUrl: nextConfig.publicRuntimeConfig.API_URL || 'http://localhost:9001',
    appName: 'expenses',
};
