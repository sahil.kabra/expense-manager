export enum ExpenseType {
    DEBIT = 'debit',
    CREDIT = 'credit',
}

export type Expense = {
    amount: number;
    date: Date;
    description?: string;
    id?: string; // this will be null for an expense that has not been saved
    labels: string[]; // metadata for the expense that could be used to search
    type: ExpenseType;
};

export type Filters = {
    labels?: string | string[];
    date?: Date;
    type?: ExpenseType;
};
