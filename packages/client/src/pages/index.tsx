import * as React from 'react';

import AddExpense from '../components/AddExpense';
import ExpenseList from '../components/ExpenseList';
import { Button, Title } from './index.style';

interface State {
    addExpenseShown: boolean;
}
class LandingPage extends React.Component<null, State> {
    public constructor() {
        super(null);
        this.state = {
            addExpenseShown: false
        };
    }
    public render() {
        return (
            <div>
                <Title>Expense Manager</Title>
                <Button onClick={this.handleAddExpense}>Add Expense</Button>
                {this.state.addExpenseShown && (
                    <AddExpense onClose={this.handleAddExpense} />
                )}
                {!this.state.addExpenseShown && <ExpenseList />}
            </div>
        );
    }
    private handleAddExpense = () => {
        this.setState({
            addExpenseShown: !this.state.addExpenseShown
        });
    };
}

export default LandingPage;
