import * as React from 'react';
import Document, {
    Head,
    Main,
    NextScript,
    NextDocumentContext
} from 'next/document';
import { ServerStyleSheet } from 'styled-components';

export interface CustomDocumentProps {
    styleTags: React.ReactElement<{}>[];
}

export default class CustomDocument extends Document<CustomDocumentProps> {
    static getInitialProps({ renderPage }: NextDocumentContext) {
        const sheet = new ServerStyleSheet();
        const page = renderPage(App => props =>
            sheet.collectStyles(<App {...props} />)
        );
        const styleTags = sheet.getStyleElement();
        return { ...page, styleTags };
    }

    render() {
        return (
            <html lang="en">
                <Head>
                    <meta
                        name="viewport"
                        content="width=device-width, initial-scale=1"
                    />
                    {/* styled-components */}
                    {this.props.styleTags}
                </Head>
                <body>
                    <Main />
                    <NextScript />
                </body>
            </html>
        );
    }
}
