const withTypescript = require('@zeit/next-typescript');
const withCSS = require('@zeit/next-css');

module.exports = withTypescript(
    withCSS({
        publicRuntimeConfig: {
            API_URL: process.env.API_URL,
        },
    })
);
